import React, { useState } from 'react';
import logo from './yslogo.png';
import './App.css';
import { Typography, Layout, Menu, Popover, Button } from 'antd';
import { PieChartOutlined, UserOutlined, DesktopOutlined, TeamOutlined, FileOutlined } from '@ant-design/icons';

const { Header, Content, Footer, Sider } = Layout;
const { Title } = Typography;
const { SubMenu } = Menu;

function App() {
	const [ collapsed, setCollapsed ] = useState(false);

	const onCollapse = (collapsed) => {
		console.log(collapsed);
		if (collapsed) setCollapsed(false);
		else setCollapsed(true);
	};
	const content = (
		<div>
			<p>Content</p>
			<p>Content</p>
		</div>
	);
	return (
		<Layout className="MainLayout">
			<Header className="MainHeader">
				<img className="logo" src={logo} />
				<Menu className="RightMenu" theme="dark" mode="horizontal" defaultSelectedKeys={[ '2' ]}>
					{/* <Popover content={content} title="Title"> */}
					<Menu.Item key="1">nav 1</Menu.Item>
					{/* </Popover> */}
				</Menu>
			</Header>
			<Layout className="MainLayout">
				<Sider
					collapsible
					// collapsed={collapsed}
					breakpoint="lg"
					collapsedWidth="0"
					onBreakpoint={(broken) => {
						console.log(broken);
					}}
					onCollapse={(collapsed, type) => {
						console.log(collapsed, type);
					}}
					className="MainSider"
				>
					<Menu theme="dark" defaultSelectedKeys={[ '1' ]} mode="inline">
						<Menu.Item key="1">
							<PieChartOutlined />
							<span>Option 1</span>
						</Menu.Item>
						<Menu.Item key="2">
							<DesktopOutlined />
							<span>Option 2</span>
						</Menu.Item>
						<SubMenu
							key="sub1"
							title={
								<span>
									<UserOutlined />
									<span>User</span>
								</span>
							}
						>
							<Menu.Item key="3">Tom</Menu.Item>
							<Menu.Item key="4">Bill</Menu.Item>
							<Menu.Item key="5">Alex</Menu.Item>
						</SubMenu>
						<SubMenu
							key="sub2"
							title={
								<span>
									<TeamOutlined />
									<span>Team</span>
								</span>
							}
						>
							<Menu.Item key="6">Team 1</Menu.Item>
							<Menu.Item key="8">Team 2</Menu.Item>
						</SubMenu>
						<Menu.Item key="9">
							<FileOutlined />File
						</Menu.Item>
					</Menu>
				</Sider>
				<Content style={{ height: '100vh' }} className="MainContent">
					main content<Button type="primary">Click me</Button>
				</Content>
			</Layout>
			<Footer className="MainFooter">footer</Footer>
		</Layout>
	);
}

export default App;
